#
# BEGIN COPYRIGHT BLOCK
# This Program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; version 2 of the License.
# 
# This Program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA.
# 
# In addition, as a special exception, Red Hat, Inc. gives You the additional
# right to link the code of this Program with code not covered under the GNU
# General Public License ("Non-GPL Code") and to distribute linked combinations
# including the two, subject to the limitations in this paragraph. Non-GPL Code
# permitted under this exception must only link to the code of this Program
# through those well defined interfaces identified in the file named EXCEPTION
# found in the source code files (the "Approved Interfaces"). The files of
# Non-GPL Code may instantiate templates or use macros or inline functions from
# the Approved Interfaces without causing the resulting work to be covered by
# the GNU General Public License. Only Red Hat, Inc. may make changes or
# additions to the list of Approved Interfaces. You must obey the GNU General
# Public License in all respects for all of the Program code and other code used
# in conjunction with the Program except the Non-GPL Code covered by this
# exception. If you modify this file, you may extend this exception to your
# version of the file, but you are not obligated to do so. If you do not wish to
# provide this exception without modification, you must delete this exception
# statement from your version and license this file solely under the GPL without
# exception. 
# 
# 
# Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
# Copyright (C) 2005 Red Hat, Inc.
# All rights reserved.
# END COPYRIGHT BLOCK
#
# Packaging nmake Makefile for Console.msi

!IF EXIST(VERSION.mak)
!INCLUDE VERSION.mak
!ENDIF

PKGDIR=.
WXSDIR=.
!IF "$(CPU)" == "AMD64"
FLAG64=_64
USE64=1
PLATFORM=x86_64
MSMPLAT=x64
!ELSE
PLATFORM=i386
MSMPLAT=x86
!ENDIF

!IF "$(APPVER)" == "6.0"
WINVER=8.0
!ELSE
WINVER=6.1
!ENDIF

!IF "$(BUILD_DEBUG)" == "optimize"
OPTDBG=_OPT
!ELSE
OPTDBG=_DBG
!ENDIF

PLAT=WINNT$(WINVER)$(FLAG64)$(OPTDBG).OBJ

FULL_PKGNAME=$(BRANDNOSPACE)-$(PKGNAME)-$(VERSION)-$(PLATFORM).msi

# could also use wget or curl
!IF EXIST("c:\program files\support tools\bitsadmin.exe")
BITSADMIN=c:\program files\support tools\bitsadmin.exe
!ELSE
BITSADMIN=bitsadmin
!ENDIF
DOWNLOAD="$(BITSADMIN)" /wrap /transfer consolebuild /download /priority normal
!IFNDEF SBC
SBC=http://filer/components
!ENDIF
!IFNDEF SBV
SBV=http://filer/components
!ENDIF

MOZILLABUILDDIR=c:\mozilla-build

#UZCMD=cscript //nologo unzip.vbs
UZCMD=$(MOZILLABUILDDIR)\info-zip\unzip.exe -q -o

NSPRVER=4.8.4
NSSVER=3_12_6
LDAPJDKVER=4.18
LDAPJDKVERDIR=v$(LDAPJDKVER)
JSSVER=4_2_6
!IFDEF USE64
JSSVERDIR=JSS_$(JSSVER)_RTM/20091019.1
!ELSE
JSSVERDIR=JSS_$(JSSVER)_RTM/20091020.1
!ENDIF
IDMVER=1.1.7
IDMVERDIR=$(IDMVER)/20110621.1
WIXVER=351728
#WIXVER=2.0.5805.0

# where to stage the package layout
PKGDIR=build.$(PLAT)
# where to find components to package
COMPDIR=$(PKGDIR)

# where to find nspr dlls
NSPRDIR=$(COMPDIR)\nspr
NSPRLIBDIR=$(NSPRDIR)\lib
# where to find nss dlls
NSSDIR=$(COMPDIR)\nss
NSSLIBDIR=$(NSSDIR)\lib
# where to find nss exes
NSSBINDIR=$(NSSDIR)\bin
# where to find jss jar and dll
JSSDIR=$(COMPDIR)\jss
# where to find the ldapjdk.jar
LDAPDIR=$(COMPDIR)\console
#LDAPDIR=$(COMPDIR)\ldapjdk-4.18
# where to find the idm-console-framework jars
IDMDIR=$(COMPDIR)\console
# where to find the console jar and other files
CONSOLEDIR=$(COMPDIR)\console
CONSOLEJARURL=$(SBV)/$(FILEPREF)-console/9.0/20110621.1

WIXURL=$(SBC)/wix
WIXDIR=$(MOZILLABUILDDIR)\wix-$(WIXVER)
#WIXDIR=$(COMPDIR)\wix
#CANDLE=$(WIXDIR)\candle.exe
CANDLE=$(WIXDIR)\candle.exe
LIGHT=$(WIXDIR)\light.exe

!IFDEF NOMSM
MSMOPTS="-dCRTMSM=" "-dPOLICYCRTMSM="
!ELSE
MSMOPTS="-dCRTMSM=$(CRTMSM)" "-dPOLICYCRTMSM=$(POLICYCRTMSM)"
!ENDIF

ALL : $(PKGDIR)\$(FULL_PKGNAME)

$(PKGDIR) :
	mkdir $@
	mkdir $@\Bitmaps

$(NSPRDIR) $(NSSDIR) $(JSSDIR) $(LDAPDIR) $(IDMDIR) $(CONSOLEDIR) $(WIXDIR):
	mkdir $@

$(NSPRDIR)\mdbinary.jar : $(NSPRDIR)
	if not exist $@ $(DOWNLOAD) $(SBC)/nspr/v$(NSPRVER)/$(PLAT)/$(@F) %%CD%%\$@

$(NSPRLIBDIR)\nspr4.dll : $(NSPRDIR)\mdbinary.jar
	$(UZCMD) $? -d $(?D)

$(NSSDIR)\mdbinary.jar : $(NSSDIR)
	if not exist $@ $(DOWNLOAD) $(SBC)/nss/NSS_$(NSSVER)_RTM/$(PLAT)/$(@F) %%CD%%\$@

$(NSSLIBDIR)\nss3.dll : $(NSSDIR)\mdbinary.jar
	$(UZCMD) $? -d $(?D)

$(JSSDIR)\jss4.dll : $(JSSDIR)
	if not exist $@ $(DOWNLOAD) $(SBV)/jss/$(JSSVERDIR)/$(PLAT)/lib/$(@F) %%CD%%\$@

$(JSSDIR)\jss4.jar :
	if not exist $@ $(DOWNLOAD) $(SBV)/jss/$(JSSVERDIR)/$(@F) %%CD%%\$@

$(LDAPDIR)\ldapjdk.jar : $(LDAPDIR)
	if not exist $@ $(DOWNLOAD) $(SBC)/ldapjdk41/$(LDAPJDKVERDIR)/$(@F) %%CD%%\$@

$(IDMDIR)\idm-console-base.jar $(IDMDIR)\idm-console-mcc.jar $(IDMDIR)\idm-console-mcc_en.jar $(IDMDIR)\idm-console-nmclf.jar $(IDMDIR)\idm-console-nmclf_en.jar : $(IDMDIR)
	if not exist $@ $(DOWNLOAD) $(SBV)/idm-console-framework/$(IDMVERDIR)/$(@F) %%CD%%\$@

$(CONSOLEDIR)\$(FILEPREF)-console_en.jar : $(CONSOLEDIR)
	if not exist $@ $(DOWNLOAD) $(CONSOLEJARURL)/$(@F) %%CD%%\$@

#$(WIXDIR)\wix-$(WIXVER).zip : $(WIXDIR)
#	if not exist $@ $(DOWNLOAD) $(WIXURL)/$(@F) %%CD%%\$@

#$(PKGDIR)\$(CANDLE) : $(WIXDIR)\wix-$(WIXVER).zip
#	$(UZCMD) $? -d $(?D)

LAYOUT : $(PKGDIR) $(NSPRLIBDIR)\nspr4.dll $(NSSLIBDIR)\nss3.dll $(JSSDIR)\jss4.dll $(JSSDIR)\jss4.jar $(LDAPDIR)\ldapjdk.jar $(IDMDIR)\idm-console-base.jar $(IDMDIR)\idm-console-mcc.jar $(IDMDIR)\idm-console-mcc_en.jar $(IDMDIR)\idm-console-nmclf.jar $(IDMDIR)\idm-console-nmclf_en.jar $(CONSOLEDIR)\$(FILEPREF)-console_en.jar
	copy /Y $(NSPRLIBDIR)\*.dll $(PKGDIR)
	copy /Y $(NSSLIBDIR)\*.dll $(PKGDIR)
	copy /Y $(NSSBINDIR)\certutil.exe $(PKGDIR)
	copy /Y $(NSSBINDIR)\pk12util.exe $(PKGDIR)
	copy /Y $(JSSDIR)\jss4.jar $(PKGDIR)
	copy /Y $(JSSDIR)\jss4.dll $(PKGDIR)
	copy /Y $(LDAPDIR)\ldapjdk.jar $(PKGDIR)
	copy /Y $(IDMDIR)\*.jar $(PKGDIR)
	copy /Y $(CONSOLEDIR)\*.jar $(PKGDIR)
# directory should already contain the bat file, icon, and bitmaps
	@(echo @echo off&echo set FILEPREF=$(FILEPREF)) > header
	copy /Y header+template.bat $(PKGDIR)\$(FILEPREF)-console.bat
	del header
	copy /Y $(ICONDIR)\*.ico $(PKGDIR)
	copy /Y $(BITMAPDIR)\* $(PKGDIR)\Bitmaps\*
	copy /Y License.rtf $(PKGDIR)

$(PKGDIR)\$(FULL_PKGNAME) : $(PKGDIR) LAYOUT $(CANDLE) $(PKGNAME).wxs
	cd $(PKGDIR) & cd & $(CANDLE) -nologo $(MSMOPTS) -dVERSION=$(VERSION) -dUSE64=$(USE64) "-dBRAND=$(BRAND)" -dBRANDNOSPACE=$(BRANDNOSPACE) "-dVENDOR=$(VENDOR)" -dPKGGUID=$(PKGGUID) "-dPRODUCTNAME=$(PRODUCTNAME)" -dFILEPREF=$(FILEPREF) -dUPGRADEGUID=$(UPGRADEGUID) -dOLDGUID=$(OLDGUID) "-dSHORTCUT=$(SHORTCUT)" "-dOLDSHORTCUT=$(OLDSHORTCUT)" "-dOLDPROGRAMFOLDER=$(OLDPROGRAMFOLDER)" -out $(PKGNAME).wixobj ..\$(PKGNAME).wxs
	$(LIGHT) -nologo -out $@ -b $(PKGDIR) $(PKGDIR)\$(PKGNAME).wixobj -ext WixUIExtension -cultures:en-us

clean:
	rmdir /S /Q $(PKGDIR)

cleanpkg:
	del $(PKGDIR)\$(FULL_PKGNAME)
	del $(PKGDIR)\$(PKGNAME).wixobj
	del $(PKGDIR)\$(FILEPREF)-console.bat

help:
	@echo You must define the following paths:
	@echo BASEDIR - the directory containing all of the other components' directories
	@echo   e.g. nmake BASEDIR="c:\Documents and Settings\userid"
	@echo Use
	@echo   nmake BASEDIR=foo download
	@echo to download the components from the default filer
	@echo the download target will create $(PKGDIR) and place the files in there
	@echo You will have to download the branded console jar file manually
	@echo Or you may download them separately and specify individual directories:
	@echo NSPRLIBDIR - directory containing the NSPR dlls
	@echo NSSLIBDIR - directory containing the NSS dlls
	@echo NSSBINDIR - directory containing certutil.exe and pk12util.exe
	@echo JSSDIR - directory containing jss4.jar and jss4.dll
	@echo LDAPDIR - directory containing ldapjdk.jar
	@echo IDMDIR - directory containing the IDM Console Framework jar files
	@echo CONSOLEDIR - directory containing the 389 console jar file
	@echo WIXDIR - directory containing the WiX candle.exe, light.exe, and library files
	@echo Type 'nmake NSPRLIBDIR=... NSSLIBDIR=... etc.' to build the Console.msi package
