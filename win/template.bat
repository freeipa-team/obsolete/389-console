echo off
rem BEGIN COPYRIGHT BLOCK
rem Copyright (C) 2005 Red Hat, Inc.
rem All rights reserved.
rem
rem This library is free software; you can redistribute it and/or
rem modify it under the terms of the GNU Lesser General Public
rem License as published by the Free Software Foundation version
rem 2.1 of the License.
rem                                                                                 
rem This library is distributed in the hope that it will be useful,
rem but WITHOUT ANY WARRANTY; without even the implied warranty of
rem MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
rem Lesser General Public License for more details.
rem                                                                                 
rem You should have received a copy of the GNU Lesser General Public
rem License along with this library; if not, write to the Free Software
rem Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
rem END COPYRIGHT BLOCK

rem set the JAVA to use here
rem set JAVA=C:\j2sdk1.4.2_15\bin\java

if not "%JAVA%foo"=="foo" goto launch

java -version > nul 2>&1 || goto findjre

set JAVA=java
goto launch

:findjre
rem look for Java Runtime Environment in registry
reg QUERY "HKLM\SOFTWARE\JavaSoft\Java Runtime Environment" > nul 2>&1 || goto findjdk

rem can we grab the java location from the registry?
rem set JAVA=path\bin\java
rem apparently not, in a batch file
rem goto launch
echo The Java Runtime Environment is installed on this machine, but the
echo command java.exe is not in your PATH.  You can either make sure java.exe
echo is in the PATH, or edit this script to set JAVA to the full path of
echo java.exe
pause
goto end

:findjdk
reg QUERY "HKLM\SOFTWARE\JavaSoft\Java Development Kit" > nul 2>&1 || goto nojava

rem can we grab the java location from the registry?
rem set JAVA=path\bin\java
rem goto launch
echo The Java Development Kit is installed on this machine, but the
echo command java.exe is not in your PATH.  You can either make sure java.exe
echo is in the PATH, or edit this script to set JAVA to the full path of
echo java.exe
pause
goto end

:nojava
echo Java does not appear to be installed on this machine.  Please download and install the Java Runtime Environment and make sure the java.exe command is in the PATH of this command.
pause
goto end

:launch
set BASEPATH=.
set BRANDCONSOLEJARDIR=%BASEPATH%
set CONSOLEJARDIR=%BASEPATH%
set JSSDIR=%BASEPATH%
set LDAPJARDIR=%BASEPATH%

set PATH=%BASEPATH%;%PATH%

rem
rem Launch the Console
rem
echo on
"%JAVA%" "-Djava.library.path=%JSSDIR%" -cp "%JSSDIR%/jss4.jar;%LDAPJARDIR%/ldapjdk.jar;%CONSOLEJARDIR%/idm-console-base.jar;%CONSOLEJARDIR%/idm-console-mcc.jar;%CONSOLEJARDIR%/idm-console-mcc_en.jar;%CONSOLEJARDIR%/idm-console-nmclf.jar;%CONSOLEJARDIR%/idm-console-nmclf_en.jar;%BRANDCONSOLEJARDIR%/%FILEPREF%-console_en.jar" -Djava.util.prefs.systemRoot=%HOME%/.%FILEPREF%-console -Djava.util.prefs.userRoot=%HOME%/.%FILEPREF%-console -Djava.net.preferIPv4Stack=true com.netscape.management.client.console.Console %*

:end
