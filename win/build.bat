@rem //
@rem // BEGIN COPYRIGHT BLOCK
@rem // This Program is free software; you can redistribute it and/or modify it under
@rem // the terms of the GNU General Public License as published by the Free Software
@rem // Foundation; version 2 of the License.
@rem // 
@rem // This Program is distributed in the hope that it will be useful, but WITHOUT
@rem // ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
@rem // FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
@rem // 
@rem // You should have received a copy of the GNU General Public License along with
@rem // this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
@rem // Place, Suite 330, Boston, MA 02111-1307 USA.
@rem // 
@rem // In addition, as a special exception, Red Hat, Inc. gives You the additional
@rem // right to link the code of this Program with code not covered under the GNU
@rem // General Public License ("Non-GPL Code") and to distribute linked combinations
@rem // including the two, subject to the limitations in this paragraph. Non-GPL Code
@rem // permitted under this exception must only link to the code of this Program
@rem // through those well defined interfaces identified in the file named EXCEPTION
@rem // found in the source code files (the "Approved Interfaces"). The files of
@rem // Non-GPL Code may instantiate templates or use macros or inline functions from
@rem // the Approved Interfaces without causing the resulting work to be covered by
@rem // the GNU General Public License. Only Red Hat, Inc. may make changes or
@rem // additions to the list of Approved Interfaces. You must obey the GNU General
@rem // Public License in all respects for all of the Program code and other code used
@rem // in conjunction with the Program except the Non-GPL Code covered by this
@rem // exception. If you modify this file, you may extend this exception to your
@rem // version of the file, but you are not obligated to do so. If you do not wish to
@rem // provide this exception without modification, you must delete this exception
@rem // statement from your version and license this file solely under the GPL without
@rem // exception. 
@rem // 
@rem // 
@rem // Copyright (C) 2005 Red Hat, Inc.
@rem // All rights reserved.
@rem // END COPYRIGHT BLOCK
@rem //

@echo off

if [%CPU%] == [AMD64] (
  set MSMPLAT=x64
) else (
  set MSMPLAT=x86
)

REM Look for the merge module containing the redistributable
REM runtime for our platform

REM on 64-bit platforms
for %%i in ("%COMMONPROGRAMFILES(X86)%\Merge Modules\"Microsoft_*_CRT_*%MSMPLAT%.msm) do set CRTMSM="%%i"
for %%i in ("%COMMONPROGRAMFILES(X86)%\Merge Modules\"policy_*Microsoft_*_CRT_*%MSMPLAT%.msm) do set POLICYCRTMSM="%%i"

REM on 32-bit platforms
for %%i in ("%COMMONPROGRAMFILES%\Merge Modules\"Microsoft_*_CRT_*%MSMPLAT%.msm) do set CRTMSM="%%i"
for %%i in ("%COMMONPROGRAMFILES%\Merge Modules\"policy_*Microsoft_*_CRT_*%MSMPLAT%.msm) do set POLICYCRTMSM="%%i"

if not defined CRTMSM (
   if not ["%MSSDK%"] == [] (
      set CRTMSM="%MSSDK%"\Redist\VC\microsoft.vcxx.crt.%MSMPLAT%_msm.msm
      set POLICYCRTMSM="%MSSDK%"\Redist\VC\policy.x.xx.microsoft.vcxx.crt.%MSMPLAT%_msm.msm
   )
)

if not defined CRTMSM (
   if not defined POLICYCRTMSM (
      echo ERROR: could not find the merge modules for the Visual C++
      echo runtime side by side assemblies - they should be provided
      echo with the Visual Studio C++ and/or the Windows SDK
      echo cannot continue
      exit 1
   )
   echo WARNING: no Visual C++ merge modules found
   echo Package will be built without merge modules
   echo In order to run the package, you will have
   echo to go to the microsoft web site, download the
   echo Visual C++ Redistributable Package, and install it
)

nmake /nologo CRTMSM=%CRTMSM% POLICYCRTMSM=%POLICYCRTMSM%
