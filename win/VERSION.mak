# this is the base name of the msi package
PKGNAME=Console
# this is the branding for the package
BRAND=389
# this it the brand used in the package filename - no spaces
BRANDNOSPACE=389
# this is the vendor or manufacturer
VENDOR=389 Project
# the version
VERSION=1.1.18
# the name of the product - this is used in the title of the
# installer, in the name of the folder, and in the name
# of the shortcuts
PRODUCTNAME=$(BRAND) Management Console
# this is the GUID of the package - must be changed
# when the version is changed - use uuidgen -n1
PKGGUID=EE78E76B-4B97-45B7-8E0A-2DD9B39A3F92
# the upgrade GUID should usually not be changed
UPGRADEGUID=7EA828C0-C219-438d-9BB3-3418DC900D60
# guid of old version to be removed
OLDGUID=DF505B7B-9D6A-4F39-8E50-A26434B05C02
OLDSHORTCUT=Fedora IDM Console.lnk
OLDPROGRAMFOLDER=Fedora Identity Management Console
# filename prefix for certain branded jar and script files
FILEPREF=389
# location of brand specific bitmaps
BITMAPDIR=Bitmaps
ICONDIR=.
# name of desktop shortcut to create
SHORTCUT=389 Console
